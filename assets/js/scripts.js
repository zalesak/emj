document.addEventListener("DOMContentLoaded", function () {

    // Modal
    const modalOverlay = document.querySelector(".modal__overlay");
    if(modalOverlay){
      modalOverlay.addEventListener("click", () => {
        const modalsArr = Array.from(document.querySelectorAll('.modal'));
        modalsArr.forEach(modal => {
          modal.style.display = 'none';
        })
      });
    }
    

    // Right top corner navs & controls
    const searchBtn = document.querySelector(".toggle-search");
    const closeBtn = document.querySelector(".close-search");
    const searchForm = document.querySelector(".search-form");

    if(searchBtn){
      searchBtn.addEventListener("click", () => {
        tasksMenu.classList.remove("show");
        notifMenu.classList.remove("show");
        settingsMenu.classList.remove("show");
        searchBtn.classList.toggle("open");
        searchForm.classList.toggle("show");
      });
    }

    if(closeBtn){
      closeBtn.addEventListener("click", () => {
        searchForm.classList.remove("show");
      });
    }

    const settingsBtn = document.querySelector(".toggle-menu-settings");
    const settingsMenu = document.querySelector("#menu-settings");

    if(settingsBtn){
      settingsBtn.addEventListener("click", () => {
        searchForm.classList.remove("show");
        tasksMenu.classList.remove("show");
        notifMenu.classList.remove("show");
        settingsBtn.classList.toggle("open");
        settingsMenu.classList.toggle("show");
      });
    }

    const notifBtn = document.querySelector(".toggle-menu-notif");
    const notifMenu = document.querySelector("#menu-notif");

    if(notifBtn){
      notifBtn.addEventListener("click", () => {
        searchForm.classList.remove("show");
        settingsMenu.classList.remove("show");
        tasksMenu.classList.remove("show");
        notifBtn.classList.toggle("open");
        notifMenu.classList.toggle("show");
      });
    }

    const tasksBtn = document.querySelector(".toggle-menu-tasks");
    const tasksMenu = document.querySelector("#menu-tasks");

    if(tasksBtn){
      tasksBtn.addEventListener("click", () => {
        searchForm.classList.remove("show");
        settingsMenu.classList.remove("show");
        notifMenu.classList.remove("show");
        tasksBtn.classList.toggle("open");
        tasksMenu.classList.toggle("show");
      });
    }

    // Show & Hide Form Field Group
    const formGroupToggle = Array.from(document.querySelectorAll(".form-group__toggle"));

    if(formGroupToggle){
      formGroupToggle.forEach(button => {
        button.addEventListener("click", (e) => {
          button.classList.toggle("closed");
          e.target.parentNode.parentNode.classList.toggle("closed");
        });
      });
    }

    // Sidebar 
    const sideToggleMenu = document.querySelector(".header__toggle");
    const headerLogo = document.querySelector(".header__logo");
    const headerContent = document.querySelector(".header__content");
    const sideMenu = document.querySelector(".sidebar");
    const contentContainer = document.querySelector(".content__container");

    let isSidebarToggled = localStorage.getItem("sidebarToggled");

    if (window.innerWidth > 1200) {
      if(isSidebarToggled === 'true') {
        sideToggleMenu.classList.toggle("open");
        headerLogo.classList.toggle("toggled");
        headerContent.classList.toggle("toggled");
        sideMenu.classList.toggle("toggled");
        contentContainer.classList.toggle("full-width");
      }   
    }

    sideToggleMenu.addEventListener("click", () => {
      isSidebarToggled = localStorage.getItem("sidebarToggled");
      if(isSidebarToggled === 'true') {
        console.log('tttrtrrr');
        localStorage.setItem("sidebarToggled", 'false');  
      } else {
        console.log('asdasd');
        localStorage.setItem("sidebarToggled", 'true');
      }
      sideToggleMenu.classList.toggle("open");
      headerLogo.classList.toggle("toggled");
      headerContent.classList.toggle("toggled");
      sideMenu.classList.toggle("toggled");
      contentContainer.classList.toggle("full-width");
    });

    // Tabs
    const tabBoxArr = Array.from(document.querySelectorAll(".tab-box"));
    const tabLinksArr = Array.from(document.querySelectorAll(".tab-link"));
    if(tabLinksArr){
      tabLinksArr.forEach(tab => {
        tab.addEventListener("click", (e) => {
          e.preventDefault();
          tabLinksArr.forEach(item => {
            item.classList.remove('active');
          })
          tabBoxArr.forEach(item => {
            item.style.display = "none";
          })
          tab.classList.toggle("active");
          const href = tab.getAttribute('href').replace("#", "");;
          const tabBox = document.getElementById(href);
          tabBox.style.display = "block";
        });
      });
    }
 
});

// Clickable row in table heading to URL in data attribute
jQuery(document).ready(function($) {
  $(".href-row").click(function() {
      window.location = $(this).data("href");
  });
});